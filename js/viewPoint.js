//按下
function viewPointFunctionDown(event) {
    var event = event || window.event;
    if (event.button == "2") {
        window.controls.enabled = false; // 控制器将不会响应用户的操作

        var intersects = raycasterFunction(event);
        if (intersects[0]) {
            console.log(intersects[0]);
            document.getElementsByClassName("viewPoint")[0].style.display = 'flex';
            document.querySelectorAll(".viewPoint .box .coords .content .X")[0].innerHTML = "x:" + intersects[0].point.x;
            document.querySelectorAll(".viewPoint .box .coords .content .Y")[0].innerHTML = "y:" + intersects[0].point.y;
            document.querySelectorAll(".viewPoint .box .coords .content .Z")[0].innerHTML = "z:" + intersects[0].point.z;

            document.querySelectorAll(".viewPoint .box .materialType .content")[0].innerHTML = intersects[0].object.material.type;

            document.querySelectorAll(".viewPoint .box .geometryType .content")[0].innerHTML = intersects[0].object.geometry.type;
        } else {
            document.getElementsByClassName("viewPoint")[0].style.display = 'none';
        }
    }
}

// 抬起
function viewPointFunctionUp(event) {
    var event = event || window.event;
    if (event.button == "2") {
        window.controls.enabled = true;
    }
}

// 获取相交的几何体信息
function raycasterFunction(event) {
    var raycaster = new THREE.Raycaster()
    var mouse = new THREE.Vector2()

    // 将鼠标点击的位置坐标转换成threejs中的标准坐标
    mouse.x = (event.clientX / document.querySelector('#model').clientWidth) * 2 - 1
    mouse.y = -(event.clientY / document.querySelector('#model').clientHeight) * 2 + 1


    // 通过鼠标点的位置和当前相机的矩阵计算出raycaster
    raycaster.setFromCamera(mouse, camera)

    // 获取raycaster直线和所有模型互交的数组集合
    var intersects = raycaster.intersectObjects(scene.children)
    return intersects
}