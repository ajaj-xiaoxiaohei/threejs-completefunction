// 隐藏
function visibleFunction(event) {
    var raycaster = new THREE.Raycaster()
    var mouse = new THREE.Vector2()

    // 将鼠标点击的位置坐标转换成threejs中的标准坐标
    mouse.x = (event.clientX / document.querySelector('#model').clientWidth) * 2 - 1
    mouse.y = -(event.clientY / document.querySelector('#model').clientHeight) * 2 + 1


    // 通过鼠标点的位置和当前相机的矩阵计算出raycaster
    raycaster.setFromCamera(mouse, camera)

    // 获取raycaster直线和所有模型互交的数组集合
    var intersects = raycaster.intersectObjects(scene.children)

    // 将所有的相交的模型进行隐藏
    for (var i = 0; i < intersects.length; i++) {
        intersects[i].object.visible = false
    }
}