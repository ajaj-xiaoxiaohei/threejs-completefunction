// 环境光开启
function ambientLightTrue() {
    if (!ambient) {
        ambient = new THREE.AmbientLight(0x666666)
        scene.add(ambient)
    }
}

// 环境光关闭
function ambientLightFalse() {
    scene.children.forEach((res) => {
        if (res.type == "AmbientLight") {
            scene.remove(res);
            ambient = null
        }
    })
}

// 点光源开启
function pointLightTrue() {
    closeLight()
    light = new THREE.PointLight(0xffffff, 1, 100)
    light.position.set(10, 15, 5)
    scene.add(light)
    threeSelectOne('PointLight')
}

// 点光源关闭
// function pointLightFalse() {
//     scene.children.forEach((res) => {
//         if (res.type == "PointLight") {
//             scene.remove(res);
//             light = null
//         }
//     })
// }

// 聚光灯开启
function spotLightTrue() {
    closeLight()
    light = new THREE.SpotLight(0xffffff)
    light.position.set(10, 15, 5)
    scene.add(light)
    threeSelectOne('SpotLight')
}

// // 聚光灯关闭
// function spotLightFalse() {
//     scene.children.forEach((res) => {
//         if (res.type == "SpotLight") {
//             scene.remove(res);
//             light = null
//         }
//     })
// }

// 平行光开启
function directinalLightTrue() {
    closeLight()
    light = new THREE.DirectionalLight(0xffffff, 0.5)
    light.position.set(10, 15, 5)
    scene.add(light)
    threeSelectOne('DirectionalLight')
}

// // 平行光关闭
// function directinalLightFalse() {
//     scene.children.forEach((res) => {
//         if (res.type == "DirectionalLight") {
//             scene.remove(res);
//             light = null
//         }
//     })
// }

// 灯光关闭
function closeLight() {
    scene.children.forEach((res) => {
        if (res.type == "PointLight" || res.type == "SpotLight" || res.type == "DirectionalLight") {
            scene.remove(res);
            light = null
        }
    })
}

// 下三中灯光,三选一处理
function threeSelectOne(str) {
    if (str == 'PointLight') {
        document.querySelectorAll(".lightBannel .lights .spotLight input")[1].checked = true
        document.querySelectorAll(".lightBannel .lights .directinalLight input")[1].checked = true
    } else if (str == 'SpotLight') {
        document.querySelectorAll(".lightBannel .lights .pointLight input")[1].checked = true
        document.querySelectorAll(".lightBannel .lights .directinalLight input")[1].checked = true
    } else if (str == 'DirectionalLight') {
        document.querySelectorAll(".lightBannel .lights .pointLight input")[1].checked = true
        document.querySelectorAll(".lightBannel .lights .spotLight input")[1].checked = true
    }
}