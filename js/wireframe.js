// 按下
function wireframeFunctionDown(event) {
    var event = event || window.event;
    if (event.button == "2") {
        window.controls.enabled = false; // 控制器将不会响应用户的操作
        var intersects = raycasterFunction(event);
        if (intersects[0]) {
            intersects[0].object.material.wireframe = true
        }
    }

}

// 抬起
function wireframeFunctionUp() {
    var event = event || window.event;
    if (event.button == "2") {
        window.controls.enabled = true;
    }
}

// 获取相交的几何体信息
function raycasterFunction(event) {
    var raycaster = new THREE.Raycaster()
    var mouse = new THREE.Vector2()

    // 将鼠标点击的位置坐标转换成threejs中的标准坐标
    mouse.x = (event.clientX / document.querySelector('#model').clientWidth) * 2 - 1
    mouse.y = -(event.clientY / document.querySelector('#model').clientHeight) * 2 + 1


    // 通过鼠标点的位置和当前相机的矩阵计算出raycaster
    raycaster.setFromCamera(mouse, camera)

    // 获取raycaster直线和所有模型互交的数组集合
    var intersects = raycaster.intersectObjects(scene.children)
    return intersects
}