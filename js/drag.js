function dragFunction() {
    isDrag = !isDrag;
    if (isDrag) {
        var objects = [];
        // 添加平移控件
        // *******仅用于显示辅助拖拽控件（可删除）********
        var transformControls = new THREE.TransformControls(camera, renderer.domElement);
        scene.add(transformControls);
        // ***************

        // 过滤不是 Mesh 的物体,例如辅助网格( 添加可拖拽的模型 )
        for (var i = 0; i < scene.children.length; i++) {
            if (scene.children[i].isGroup) {
                scene.children[i].children.forEach(res => {
                    objects.push(res)
                })
            }
        }

        // 初始化拖拽控件
        var dragControls = new THREE.DragControls(objects, camera, renderer.domElement);

        // 鼠标略过事件
        dragControls.addEventListener('hoveron', (event) => {
            // ******仅用于显示辅助拖拽控件（可删除）******
            // 让变换控件对象和选中的对象绑定
            transformControls.attach(event.objects);
            // ************
        })

        // 开始拖拽
        dragControls.addEventListener('dragstart', (event) => {
            controls.enabled = false;
        })

        // 拖拽结束
        dragControls.addEventListener('dragend', (event) => {
            controls.enabled = true
        })
    } else {
        init()
    }
}