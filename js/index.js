var scene = null
var camera = null
var renderer = null
var id = null
var stat = null
    // var controls = null
window.controls = null
var cameraMetrix = [0, 25, 35]
var clipNum = 10
var PlaneArr = []
var helper = null
var isClip = false
var axesHelper = null
var isDrag = false
var isMeasure = false
var aMeasure = null
var isOpacity = false
var aOpacity = null
var isSelect = false
var aSelect = null
window.composer = null
window.outlinePass = null
window.renderPass = null
var isBgSwitch = false
var isDirection = true
var isWander = false
var curve = null
var time = 0
var isWireframe = false
var aWireframe = null
var isLight = false
var ambient = null
var light = null
var isViewPoint = false
var aViewPoint = null
var isSpriteSheet = false

var self = null
var model = null
var isDyeing = false
var aDyeing = null
var aVisible = null
var isVisible = false
var isSign = false
var aSign = null

// 初始化
function init() {
    // 判断helper是否存在（用于裁剪平面）
    if (helper) {
        scene.remove(helper)
    }

    // 不同浏览器的适配
    var requestAnimationFrame = window.requestAnimationFrame ||
        window.mozRequestAnimationFrame ||
        window.webkitRequestAnimationFrame ||
        window.msRequestAnimationFrame;
    window.requestAnimationFrame = requestAnimationFrame;

    // 渲染器 - 绑定到以存在的canvas节点
    const canvas = document.querySelector('#canvas')
    canvas.width = document.querySelector('#model').clientWidth
    canvas.height = document.querySelector('#model').clientHeight
    renderer = new THREE.WebGLRenderer({
        canvas,
        antialias: true // 是否执行抗锯齿
    })
    renderer.setClearColor(0xffffff)

    // 场景
    scene = new THREE.Scene()

    // 辅助三维线
    axesHelper = new THREE.AxesHelper(800);
    scene.add(axesHelper);

    // 照相机
    camera = new THREE.PerspectiveCamera(45, 4 / 3, 1, 1000)
    camera.lookAt(0, 0, 0)
    camera.position.set(cameraMetrix[0], cameraMetrix[1], cameraMetrix[2]) // 需调整
    scene.add(camera)

    // 添加光照
    light = new THREE.PointLight(0xffffff, 1, 100)
    light.position.set(10, 15, 5)
    scene.add(light)

    // 环境光
    ambient = new THREE.AmbientLight(0x666666)
    scene.add(ambient)

    // 模型导入
    importModel()

    // 定时器
    requestAnimationFrame(draw)

    // 鼠标操作三维场景
    window.controls = new THREE.OrbitControls(camera, renderer.domElement); //创建控件对象
    window.controls.addEventListener('change', render);
    window.controls.update()

    // 监听窗口的变化 - 自适应
    window.addEventListener('resize', onWindowResize);
}

// 监听controls的变化
function render() {
    // console.log("===controls变化===>");
}

function onWindowResize() {
    camera.aspect = document.querySelector('#model').clientWidth / document.querySelector('#model').clientHeight; // 相机视口大小
    camera.updateProjectionMatrix(); // 相机更新渲染
    renderer.setSize(document.querySelector('#model').clientWidth, document.querySelector('#model').clientHeight);
    window.controls.update()
}

function draw() {
    renderer.render(scene, camera);
    controls.update();

    // 用于“选择”高亮
    if (window.composer) {
        window.composer.render()
    }

    // 漫游设置
    if (isWander) {
        moveCamera();
    }

    requestAnimationFrame(draw);
}

// 外部模型导入 - glb
function importModel() {
    const loader = new THREE.GLTFLoader(); // 写法特殊，需注意
    loader.load('../models/glb/403.glb', (gltf) => {
        model = gltf.scene;
        // model.position.set( 1, 1, 0 );
        model.scale.set(1.3, 1.3, 1.3);
        model.rotation.y = -Math.PI / 2;
        scene.add(model);
    })


}

// 不同观察视角 - 通过改变相机的位置和再次初始化
// 上
function viewTop() {
    cameraMetrix = [0, 40, 0]
    init()
}

// 下
function viewBottom() {
    cameraMetrix = [0, -40, 0]
    init()
}

// 左
function viewLeft() {
    cameraMetrix = [-30, 10, 0]
    init()
}

// 右
function viewRight() {
    cameraMetrix = [30, 10, 0]
    init()
}

// 前
function viewFront() {
    cameraMetrix = [0, 25, 35]
    init()
}

// 后
function viewBack() {
    cameraMetrix = [0, 25, -35]
    init()
}


// 裁剪
// 裁剪实际功能代码
function clipOperatioin() {
    // 裁剪
    PlaneArr = [
        // 创建一个垂直x轴的平面，方向沿着x轴负方向，沿着x轴正方向平移20
        new THREE.Plane(new THREE.Vector3(1, 0, 0), clipNum)
    ]
    renderer.clippingPlanes = PlaneArr

    // 可视化显示裁剪平面
    if (helper) {
        scene.remove(helper)
    }
    helper = new THREE.PlaneHelper(PlaneArr[0], 25, 0xFF0000)
    helper.name = 'helperClip'
    scene.add(helper)
}

// 裁剪 - 触发函数
function clip() {
    isClip = !isClip
    if (isClip) {
        clipNum = 25 // 裁剪平面位置初始化
        clipOperatioin()
            // 监听鼠标移动
        document.onmousedown = function(event) {
            // event = event || window.event;
            // var x = event.clientX;
            // var y = event.clientY;
            // var mv = new THREE.Vector3(
            //     (x / window.innerWidth) * 2 - 1, -(y / window.innerHeight) * 2 + 1,
            //     0.5);
            clipNum -= 1
            clipOperatioin()
        }
    } else {
        document.onmousedown = null // 移除鼠标点击事件
        clipNum = 25 // 裁剪平面位置初始化
        renderer.clippingPlanes = [] // 可视化显示裁剪平面 - 参数初始化
        init() // 初始化页面
    }
}

// 染色 - 触发函数
function dyeing() {
    isDyeing = !isDyeing
    aDyeing = document.getElementById('model')
    if (isDyeing) {
        aDyeing.addEventListener('contextmenu', dyeingFunction)
    } else {
        aDyeing.removeEventListener('contextmenu', dyeingFunction)
        init()
    }
}

// 拖拽 - 触发函数
function drag() {
    dragFunction()
}

// 隐藏 - 触发函数
function visible() {
    isVisible = !isVisible
    aVisible = document.getElementById('model')
    if (isVisible) {
        aVisible.addEventListener('contextmenu', visibleFunction)
    } else {
        aVisible.removeEventListener('contextmenu', visibleFunction)
        init()
    }
}

// 标识 - 触发函数 
function sign() {
    isSign = !isSign
    aSign = document.getElementById('model')
    if (isSign) {
        aSign.addEventListener('contextmenu', signFunction)
    } else {
        aSign.removeEventListener('contextmenu', signFunction)
        init()
    }
}

// 测量 - 触发函数
function measure() {
    isMeasure = !isMeasure
    aMeasure = document.getElementById('model')
    if (isMeasure) {
        aMeasure.addEventListener('mousedown', measureFunctionDown)
        aMeasure.addEventListener('mouseup', measureFunctionUp)
    } else {
        aMeasure.removeEventListener('mousedown', measureFunctionDown)
        aMeasure.removeEventListener('mouseup', measureFunctionUp)
        init()
    }
}

// 透明 - 触发函数
function opacity() {
    isOpacity = !isOpacity
    aOpacity = document.getElementById('model')
    if (isOpacity) {
        aOpacity.addEventListener('mousedown', opacityFunctionDown)
        aOpacity.addEventListener('mouseup', opacityFunctionUp)
    } else {
        aOpacity.removeEventListener('mousedown', opacityFunctionDown)
        aOpacity.removeEventListener('mouseup', opacityFunctionUp)
        init()
    }
}

// 选择 - 触发函数
function select() {
    isSelect = !isSelect
    aSelect = document.getElementById('model')
    if (isSelect) {
        aSelect.addEventListener('mousedown', selectFunctionDown)
        aSelect.addEventListener('mouseup', selectFunctionUp)
    } else {
        window.composer = null; // 此步必要，否则会画面出问题
        aSelect.removeEventListener('mousedown', selectFunctionDown)
        aSelect.removeEventListener('mouseup', selectFunctionUp)
        init()
    }
}

// 背景色切换
function backgroundSwitch() {
    isBgSwitch = !isBgSwitch
    if (isBgSwitch) {
        renderer.setClearColor(0x000000);
    } else {
        renderer.setClearColor(0xffffff);
    }

}

// 方向 - 触发函数
function direction() {
    isDirection = !isDirection;
    var directionBtns = document.getElementById("direciontBtn");
    if (isDirection) {
        directionBtns.style.display = 'none';
    } else {
        directionBtns.style.display = 'block';
    }
}

// 截图 - 触发函数
function printscreen() {
    var canvas = renderer.domElement;
    renderer.render(scene, camera);
    var imgUrl = canvas.toDataURL("image/png");
    downLoad(imgUrl);
}

// 下载文件
function downLoad(url) {
    let fd = document.createElement('a');
    fd.download = 'threejs'; //默认名是下载
    fd.href = url;
    document.body.appendChild(fd);
    fd.click();
    fd.remove();

    // // 文件下载完之后，重新渲染建模
    // setTimeout(() => {
    //     init();
    // }, 1000);
}

// 漫游 - 触发函数
function wander() {
    isWander = !isWander;
    if (isWander) {
        // 关闭手动控制
        window.controls.enabled = false;

        // 点的坐标数据
        const pointArr = [0, 20, 40, -20, 20, 0,
            0, 30, -15,
            20, 20, 0
        ]

        // 根据坐标数组转为点数组
        var points = [];
        for (var i = 0; i < pointArr.length; i += 3) {
            points.push(
                new THREE.Vector3(
                    pointArr[i],
                    pointArr[i + 1],
                    pointArr[i + 2]
                )
            )
        }

        // 创建曲线
        curve = new THREE.CatmullRomCurve3(points);
    } else {
        // 开启手动控制
        window.controls.enabled = true;
    }
}

function moveCamera() {
    if (curve) {
        // 把曲线分割成2999段，可以得到3000个点
        var points = curve.getPoints(1000);

        // 更新取点索引
        time += 1;

        // 相机所在点索引
        const index1 = time % 1000;

        // 根据索引取点
        var point = points[index1];

        // 修改相机位置
        if (point && point.x) {
            // 动态改变照相机的位置
            camera.position.set(point.x, 20, point.z);

            camera.lookAt(0, 0, 0); // 照相机看向中心点
        }
    }
}

// 线框 - 触发函数
function wireframe() {
    isWireframe = !isWireframe
    aWireframe = document.getElementById('model')
    if (isWireframe) {
        aWireframe.addEventListener('mousedown', wireframeFunctionDown)
        aWireframe.addEventListener('mouseup', wireframeFunctionUp)
    } else {
        aWireframe.removeEventListener('mousedown', wireframeFunctionDown)
        aWireframe.removeEventListener('mouseup', wireframeFunctionUp)
        init()
    }
}

// 灯光 - 触发函数
function lights() {
    isLight = !isLight;
    var light = document.getElementsByClassName('lightBannel')[0]

    if (isLight) {
        light.style.display = 'flex';

        // 初始化环境光radio
        if (ambient) {
            document.querySelectorAll(".lightBannel .lights .ambientLight input")[0].checked = true
        } else {
            document.querySelectorAll(".lightBannel .lights .ambientLight input")[1].checked = true
        }

        // 初始化点光源radio
        if (light) {
            document.querySelectorAll(".lightBannel .lights .pointLight input")[0].checked = true
        } else {
            document.querySelectorAll(".lightBannel .lights .pointLight input")[1].checked = true
        }

        // 初始化聚光灯radio
        document.querySelectorAll(".lightBannel .lights .spotLight input")[1].checked = true

        // 初始化平行光radio
        document.querySelectorAll(".lightBannel .lights .directinalLight input")[1].checked = true

    } else {
        light.style.display = 'none';
    }
}

// 精灵图 - 触发函数
function spriteSheet() {
    isSpriteSheet = !isSpriteSheet
    if (isSpriteSheet) {
        // 会议室
        var material = new THREE.SpriteMaterial({
            map: new THREE.CanvasTexture(generateSprite("会议室", 'red'))
        })

        var wall = new THREE.Sprite(material);
        wall.position.set(-9.289254620269755, 5, 4.763116518103339);
        scene.add(wall);

        // 办公区
        var material2 = new THREE.SpriteMaterial({
            map: new THREE.CanvasTexture(generateSprite("办公区", 'red'))
        })

        var wall2 = new THREE.Sprite(material2);
        wall2.position.set(5.216711493909145, 5, -3.2592571526161094);
        scene.add(wall2);

        // 前台
        var material3 = new THREE.SpriteMaterial({
            map: new THREE.CanvasTexture(generateSprite("前台", 'red'))
        })

        var wall3 = new THREE.Sprite(material3);
        wall3.position.set(-10.378538967570261, 5, -3.9219875646471465);
        scene.add(wall3);

        // boss办公室
        var material4 = new THREE.SpriteMaterial({
            map: new THREE.CanvasTexture(generateSprite("boss办公室", 'red'))
        })

        var wall4 = new THREE.Sprite(material4);
        wall4.position.set(1.4375477670915655, 5, 3.9996216592543408);
        scene.add(wall4);
    } else {
        init();
    }
}

// 生成精灵材质
function generateSprite(text, style) {
    var canvas = document.createElement('canvas');
    canvas.width = 2200;
    canvas.height = 1000;

    var context = canvas.getContext('2d');
    context.beginPath();
    context.font = '400px Microsoft YaHei';
    context.fillStyle = style;
    context.fillText(text, 0, 700);
    context.fill();
    context.stroke();
    return canvas;
}

// 视点 - 触发函数
function viewPoint() {
    isViewPoint = !isViewPoint
    aViewPoint = document.getElementById('model')
    var viewPoint = document.getElementsByClassName("viewPoint")[0]
    if (isViewPoint) {
        aViewPoint.addEventListener('mousedown', viewPointFunctionDown)
        aViewPoint.addEventListener('mouseup', viewPointFunctionUp)
    } else {
        aViewPoint.removeEventListener('mousedown', viewPointFunctionDown)
        aViewPoint.removeEventListener('mouseup', viewPointFunctionUp)
        viewPoint.style.display = 'none';
    }
}