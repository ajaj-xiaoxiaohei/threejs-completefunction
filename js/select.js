// 按下
function selectFunctionDown(event) {
    var event = event || window.event;
    if (event.button == "2") {
        window.controls.enabled = false; // 控制器将不会响应用户的操作
        var intersects = raycasterFunction(event);
        if (intersects[0]) {
            outlineObj([intersects[0].object]);
        }

    }
}

// 抬起
function selectFunctionUp(event) {
    var event = event || window.event;
    if (event.button == "2") {
        window.controls.enabled = true;
    }
}

// 获取相交的几何体信息
function raycasterFunction(event) {
    var raycaster = new THREE.Raycaster()
    var mouse = new THREE.Vector2()

    // 将鼠标点击的位置坐标转换成threejs中的标准坐标
    mouse.x = (event.clientX / document.querySelector('#model').clientWidth) * 2 - 1
    mouse.y = -(event.clientY / document.querySelector('#model').clientHeight) * 2 + 1


    // 通过鼠标点的位置和当前相机的矩阵计算出raycaster
    raycaster.setFromCamera(mouse, camera)

    // 获取raycaster直线和所有模型互交的数组集合
    var intersects = raycaster.intersectObjects(scene.children)
    return intersects
}


//高亮显示模型（呼吸灯）
function outlineObj(selectedObjects) {
    var width = document.querySelector('#model').clientWidth;
    var height = document.querySelector('#model').clientHeight;
    // 创建一个EffectComposer（效果组合器）对象，然后在该对象上添加后期处理通道。
    window.composer = new THREE.EffectComposer(renderer)
        // 新建一个场景通道  为了覆盖到原理来的场景上
    window.renderPass = new THREE.RenderPass(scene, camera)
    window.composer.addPass(window.renderPass);
    // 物体边缘发光通道
    window.outlinePass = new THREE.OutlinePass(new THREE.Vector2(width, height), scene, camera, selectedObjects)
    window.outlinePass.selectedObjects = selectedObjects
    window.outlinePass.edgeStrength = 10.0 // 边框的亮度
    window.outlinePass.edgeGlow = 1 // 光晕[0,1]
    window.outlinePass.usePatternTexture = false // 是否使用父级的材质
    window.outlinePass.edgeThickness = 2.0 // 边框宽度
    window.outlinePass.downSampleRatio = 1 // 边框弯曲度
    window.outlinePass.pulsePeriod = 5 // 呼吸闪烁的速度
    window.outlinePass.visibleEdgeColor.set(parseInt(0xFF0000)) // 呼吸显示的颜色
    window.outlinePass.hiddenEdgeColor = new THREE.Color(0, 0, 0) // 呼吸消失的颜色
    window.outlinePass.clear = true
    window.composer.addPass(window.outlinePass)
        // 自定义的着色器通道 作为参数
    var effectFXAA = new THREE.ShaderPass(THREE.FXAAShader)
    effectFXAA.uniforms.resolution.value.set(1 / width, 1 / height)
    effectFXAA.renderToScreen = true
    window.composer.addPass(effectFXAA)
}