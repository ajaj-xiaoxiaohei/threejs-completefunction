// 标识功能函数
function signFunction() {
    var raycaster = new THREE.Raycaster()
    var mouse = new THREE.Vector2()

    // 将鼠标点击的位置坐标转换成threejs中的标准坐标
    mouse.x = (event.clientX / document.querySelector('#model').clientWidth) * 2 - 1
    mouse.y = -(event.clientY / document.querySelector('#model').clientHeight) * 2 + 1


    // 通过鼠标点的位置和当前相机的矩阵计算出raycaster
    raycaster.setFromCamera(mouse, camera)

    // 获取raycaster直线和所有模型互交的数组集合
    var intersects = raycaster.intersectObjects(scene.children)

    // 将所有的相交的模型，进行标识
    const geometry = new THREE.ConeGeometry(0.1, 0.3, 32);
    const material = new THREE.MeshBasicMaterial({ color: 0xffff00 });
    var cone = new THREE.Mesh(geometry, material);
    cone.name = 'sign'
    if (intersects[0]) {
        cone.position.set(intersects[0].point.x, intersects[0].point.y, intersects[0].point.z)
        scene.add(cone);
    }
}