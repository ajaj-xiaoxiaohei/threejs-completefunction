var firstPoint = null;
var secondPoint = null;
// 鼠标按下
function measureFunctionDown(event) {
    var event = event || window.event;
    if (event.button == "2") {
        window.controls.enabled = false; // 控制器将不会响应用户的操作

        // 获取相交几何体信息
        var intersects = raycasterFunction(event);

        // 创建点
        createPoint(intersects, 'first');

        // 保存第一个点的位置信息，用于构造线
        firstPoint = intersects[0];
    }
}

// 鼠标抬起
function measureFunctionUp(event) {
    var event = event || window.event;
    if (event.button == "2") {
        window.controls.enabled = true;

        // 获取相交几何体信息
        var intersects = raycasterFunction(event);

        // 保存第二个点的位置信息，用于构造线
        secondPoint = intersects[0];

        // 创建点
        createPoint(intersects, 'second');

        // 创建两点连接线
        createLine();
    }
}

// 获取相交的几何体信息
function raycasterFunction(event) {
    var raycaster = new THREE.Raycaster()
    var mouse = new THREE.Vector2()

    // 将鼠标点击的位置坐标转换成threejs中的标准坐标
    mouse.x = (event.clientX / document.querySelector('#model').clientWidth) * 2 - 1
    mouse.y = -(event.clientY / document.querySelector('#model').clientHeight) * 2 + 1


    // 通过鼠标点的位置和当前相机的矩阵计算出raycaster
    raycaster.setFromCamera(mouse, camera)

    // 获取raycaster直线和所有模型互交的数组集合
    var intersects = raycaster.intersectObjects(scene.children)
    return intersects
}


// 创建点
function createPoint(intersects, str) {
    var vertices = []
    if (intersects[0]) {
        var x = intersects[0].point.x;
        var y = intersects[0].point.y;
        var z = intersects[0].point.z;
        vertices.push(x, y, z);
        var geometry = new THREE.BufferGeometry();
        geometry.setAttribute('position', new THREE.Float32BufferAttribute(vertices, 3));
        var material = new THREE.PointsMaterial({
            color: 0xff0000
        });
        var points = new THREE.Points(geometry, material);
        points.name = str
        scene.add(points);
    } else {
        // 如果第二个点的位置未获取到，则删除第一个已经创建的点
        if (scene.children[scene.children.length - 1].name == 'first') {
            scene.children.pop()
        }
    }
}

// 创建连接线
function createLine() {
    if (firstPoint && secondPoint) {
        var a = firstPoint.point;
        var b = secondPoint.point;
        const material = new THREE.LineBasicMaterial({
            color: 0x0000ff
        });

        const points = [];
        points.push(new THREE.Vector3(a.x, a.y, a.z));
        points.push(new THREE.Vector3(b.x, b.y, b.z));

        const geometry = new THREE.BufferGeometry().setFromPoints(points);

        const line = new THREE.Line(geometry, material);

        // 创建线之间的标签
        createTip(line);

        scene.add(line);
    }
}

// 创建线之间的标签
function createTip(line) {
    if (firstPoint && secondPoint) {
        var centerX = null;
        var centerY = null;
        var centerZ = null;

        // 第一个点的三维坐标
        var firstX = firstPoint.point.x;
        var firstY = firstPoint.point.y;
        var firstZ = firstPoint.point.z;

        // 第二点的三维坐标
        var secondX = secondPoint.point.x;
        var secondY = secondPoint.point.y;
        var secondZ = secondPoint.point.z;

        centerX = (firstX + secondX) / 2;
        centerY = (firstY + secondY) / 2;
        centerZ = (firstZ + secondZ) / 2;

        var x = centerX;
        var y = centerY;
        var z = centerZ;
        createLabel(x, y, z, line);
    }
}

// 创建标签 --- 待优化（未完成）
function createLabel(x, y, z, line) {
    // var labelDiv = document.createElement('div');
    // labelDiv.className = 'label_name';
    // labelDiv.textContent = 'x:' + x + ',y:' + y + 'z:' + z;
    // var pointLabel = new THREE.CSS2DObject(labelDiv);
    // line.add(pointLabel);
}